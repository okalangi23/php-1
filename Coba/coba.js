// Soal 1
for (let increment = 0; increment < 10; increment++) {
  console.log(increment);
}

// Soal 2
console.log("================================\nSoal 2");
for (let ganjil = 1; ganjil < 10; ganjil += 2) {
  console.log(ganjil);
}

// Soal 3
console.log("================================\nSoal 3");
for (let genap = 0; genap < 10; genap += 2) {
  console.log(genap);
}

//Soal 4
console.log("================================\nSoal 4");
let array1 = [1, 2, 3, 4, 5, 6];
console.log(array1[4]);

//Soal 5
console.log("================================\nSoal 5");
let array2 = [5, 2, 4, 1, 3, 5];
array2.sort();
console.log(array2);

// Soal 6
console.log("================================\nSoal 6\n");
let array3 = [
  "selamat",
  "anda",
  "melakukan",
  "perulangan",
  "array",
  "dengan",
  "for",
];
for (var iteration = 0; iteration < array3.length; iteration++) {
  //declare iteration = 0, nilai iterasi harus kurang dari panjang array, iterasi+=1
  console.log(array3[iteration]); //max array3.length = 7
}

//Soal 7
console.log("================================\nSoal 7");
let array4 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
let genap = [];

for (var even = 0; even < array4.length; even++) {
  if (array4[even] % 2 == 0) {
    genap.push(array4[even]);
  }
}
console.log(genap.join("\n"));

// Soal 8
console.log("================================\nSoal 8");
let kalimat = ["saya", "sangat", "senang", "belajar", "javascript"];
console.log(kalimat.join(" "));

// Soal 9
console.log("================================\nSoal 9");
var sayuran = [];
sayuran.push(
  "Kangkung",
  "Bayam",
  "Buncis",
  "Kubis",
  "Timun",
  "Seledri",
  "Tauge"
);
console.log(sayuran);
